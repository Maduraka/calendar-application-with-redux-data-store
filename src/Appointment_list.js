import React, { Component } from 'react';
import { connect } from 'react-redux';
import Appointment from './Appointment';
import EditAppointment from './Edit_appointment';
import { Spinner } from 'reactstrap';
import {DatetimePickerTrigger} from 'rc-datetime-picker';
class AllPost extends Component {
  
render() {
  
return (
<div>
  <h2 className="post_heading">APPOINTMENT LIST</h2>
  {this.props.posts.map((post) => (
  <div key={post.id}>
    {post.editing ? <EditAppointment post={post} key={post.id} /> : <Appointment post={post}
    key={post.id} />}
  </div>
))}
</div>
);
}
}
function datecon(strDateTime){
  const myDate = new Date(strDateTime);
  alert(myDate.toLocaleString());
}

const mapStateToProps = (state) => {
return {
posts: state
}
}
export default connect(mapStateToProps)(AllPost);
