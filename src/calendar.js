import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

import moment from 'moment';
import {DatetimePicker} from 'rc-datetime-picker';
class Calandar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      moment: moment(),
    };

    this.toggle = this.toggle.bind(this);
  }
  handleChange = (moment) => {
    this.setState({
      moment
    });
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
  

  render() {
    return (
      <div>
        <Button color="danger" onClick={this.toggle}>Calandar</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className} Style="padding: 7rem">
          <ModalHeader toggle={this.toggle}>Calender</ModalHeader>
          <ModalBody>
          <DatetimePicker
        moment={this.state.moment}
        onChange={this.handleChange}
      /> </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggle}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

export default Calandar;