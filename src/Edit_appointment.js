
import React, { Component } from 'react';
import { connect } from 'react-redux';


class EditAppointment extends Component {
handleEdit = (e) => {
  e.preventDefault();
  const newSelectdate = this.getSelectDate.value;
  const newReason = this.getReason.value;
  const data = {
    newSelectdate,
    newReason
  }
  this.props.dispatch({ type: 'UPDATE', id: this.props.post.id, data: data })
}
render() {
return (
<div key={this.props.post.id} className="post">
  <form className="form" onSubmit={this.handleEdit}>
    <input required type="text" ref={(input) => this.getSelectDate = input}
    defaultValue={this.props.post.selectdate} placeholder="Enter Post Title" /><br /><br />
    <textarea required rows="5" ref={(input) => this.getReason = input}
    defaultValue={this.props.post.reason} cols="28" placeholder="Enter Post" /><br /><br />
    <button>Update</button>
  </form>
</div>
);
}
}
export default connect()(EditAppointment);