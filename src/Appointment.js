import React, { Component } from 'react';
import { connect } from 'react-redux';
class Appointment extends Component {
render() {
return (
<div className="post">
  <h2 className="post_title">{this.props.post.selectdate}</h2>
  <p className="post_message">{this.props.post.reason}</p>
  <div className="control-buttons">
    <button className="edit"
    onClick={() => this.props.dispatch({ type: 'EDIT_APPOINTMENT', id: this.props.post.id })
    }
    >Edit</button>
    <button className="delete"
    onClick={() => this.props.dispatch({ type: 'DELETE_APPOINTMENT', id: this.props.post.id })}
    >Delete</button>
  </div>
</div>
);
}
}
export default connect()(Appointment);