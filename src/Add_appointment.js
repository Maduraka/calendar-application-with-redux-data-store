import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Button,Input,Col, FormGroup , Form,Label,Card } from 'reactstrap';
import moment from 'moment';
import {DatetimePickerTrigger} from 'rc-datetime-picker';
import "rc-datetime-picker/dist/picker.css";
import { API_BASE_URL } from "./constatnts";
// import { Button } from 'reactstrap';


class AddAppointment extends Component {
    constructor() {
        super();
        this.state = {
          moment: moment()
        };
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      handleSubmit = (e) => {
        e.preventDefault();
        const selectdate = this.getSelectDate.value;
        const reason =  this.getReason.value;
        const data = {
          id: new Date(),
          selectdate,
          reason,
          editing: false
        }
        const data1 = {selectdate,reason}
    console.log("ghghug" + JSON.stringify(data1));
    fetch(`${API_BASE_URL}/appointment_data`, {
      method: 'POST',
      body: JSON.stringify(data1),
      headers:{
        'Content-Type': 'application/json'
        },
    }).then(function(response) {
      return response.json();});
        
        console.log(data)
        this.props.dispatch({
          type:'ADD_APPOINTMENT',
          data});
        this.getSelectDate.value = '';
        this.getReason.value = '';

      }
      handleChange = (moment) => {
        this.setState({
          moment
        });
      }
    
      render() {
        const shortcuts = {
          'Today': moment(),
          'Yesterday': moment().subtract(1, 'days'),
          'Clear': ''
        };
    
        return (  
           <div className="post-container">
           <h3 className="post_heading">ADD APPOINTMENT</h3>
           <form className="form" onSubmit={this.handleSubmit} >
           <DatetimePickerTrigger
            shortcuts={shortcuts} 
            moment={this.state.moment}
            onChange={this.handleChange}><input required type="text" value={this.state.moment.format('YYYY-MM-DD HH:mm')}  ref={(input) => this.getSelectDate = input}
            placeholder="Select date" /></DatetimePickerTrigger>
          <br /><br />
            <textarea required rows="5" ref={(input) => this.getReason = input}
            cols="28" placeholder="Add Reason" /><br /><br />
            <button>Post</button>
           </form>
         </div>
        );
      }
    }
    

export default connect()(AddAppointment);
