const AddAppointmentReducer = (state = [], action) => {
    switch(action.type) {
      case 'ADD_APPOINTMENT':
      return state.concat([action.data])
      case 'DELETE_APPOINTMENT':
      return state.filter((post) => post.id !== action.id)
      case 'EDIT_APPOINTMENT':
      return state.map((post) => post.id === action.id ? { ...post, editing: !post.editing } : post)
      case 'UPDATE_APPOINTMENT':
      return state.map((post) => {
      if (post.id === action.id) {
      return {
      ...post,
      selectdate: action.data.newSelectdate,
      reason: action.data.newReson,
      editing: !post.editing
      }
      } else return post;
      })
      default:
      return state;
      }
      }

  export default AddAppointmentReducer;